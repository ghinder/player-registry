package de.spaceassault.playerregistry;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table( name = "player")
public class Player {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column( name = "email", nullable = false)
    private String email;

    @Column( name = "password", nullable = false)
    private String password;

    @Column( name = "name", nullable = false)
    private String name;

    @Column( name = "species", nullable = false)
    private String species;
}
