package de.spaceassault.playerregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories
public class PlayerRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlayerRegistryApplication.class, args);
	}

}
