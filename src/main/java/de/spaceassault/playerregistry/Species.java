package de.spaceassault.playerregistry;

public enum Species {
    ASSAMERER, BLOODANCER, SCIENCER;
}
