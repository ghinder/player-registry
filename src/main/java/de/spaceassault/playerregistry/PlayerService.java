package de.spaceassault.playerregistry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;

@Service
@Transactional
public class PlayerService {

    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository){
        this.playerRepository = playerRepository;
    }

    public Player registerNewPlayerAccount(RegistrationDto accountDto) throws EmailExistsException{
        if (emailExists(accountDto.getEmail())) {
            throw new EmailExistsException(
                    "There is an account with that email address:" +
                            " accountDto.getEmail()");
        }
        Player user = new Player();
        user.setName(accountDto.getName());
        user.setPassword(accountDto.getPassword());
        user.setEmail(accountDto.getEmail());
        return playerRepository.save(user);
    }

    private boolean emailExists(String email) {
        Player user = playerRepository.findByEmail(email);
        if (user != null) {
            return true;
        }
        return false;
    }
}
