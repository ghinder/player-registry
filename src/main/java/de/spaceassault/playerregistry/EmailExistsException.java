package de.spaceassault.playerregistry;

public class EmailExistsException extends Throwable {

    public EmailExistsException(String message) {
        super(message);
    }
}
